#!/usr/bin/python
# -*- coding: utf-8 -*-

# all duration results are in milliseconds

# postgresql-10-postgis-2.4 and python-psutil must be installed

# postgres must be able to restart postgresql with no password:
# use "visudo -f /etc/sudoers.d/postgres" and enter the following line :
# postgres ALL=(ALL) NOPASSWD: /bin/systemctl restart postgresql

import optparse,logging,shlex,subprocess,math,os,shutil,re,psutil,signal
import multiprocessing as mp
import datetime as dt

LOG_LEVEL=logging.INFO

logger = logging.getLogger('test_sig')
logger.setLevel(LOG_LEVEL)
logger_ch = logging.StreamHandler()
formatter = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logger_ch.setFormatter(logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
logger_ch.setLevel(LOG_LEVEL)
logger.addHandler(logger_ch)

log_bash_cmd = False

original_queries = [
    'SELECT *, ST_IsValidDetail(geom) FROM s_adr.code_insee_code_postal WHERE ST_IsValid(geom) = false;',
    'SELECT * FROM S_GEO.COMMUNE WHERE ST_Area(geom) = (SELECT MAX(ST_Area(GEOM)) FROM S_GEO.COMMUNE)',
    'SELECT CODE_DEPT, ST_Union(GEOM) FROM S_GEO.COMMUNE GROUP BY CODE_DEPT;',
    "WITH POINTS AS (SELECT CAST('POINT ( 430354.933 6623007.700)' AS geometry) AS P, 1 AS N UNION ALL SELECT CAST('POINT ( 980190.133 6333111.233)' AS geometry), 2 UNION ALL SELECT CAST('POINT ( 574865.267 6909297.167)' AS geometry), 3 UNION ALL SELECT CAST('POINT ( 501587.200 6548318.933)' AS geometry), 4 UNION ALL SELECT CAST('POINT ( 444282.067 6251421.667)' AS geometry), 5 UNION ALL SELECT CAST('POINT ( 460953.333 6775817.633)' AS geometry), 6 UNION ALL SELECT CAST('POINT (1032008.400 6323716.133)' AS geometry), 7 UNION ALL SELECT CAST('POINT ( 875328.767 6866887.500)' AS geometry), 8 UNION ALL SELECT CAST('POINT ( 666398.867 6560452.500)' AS geometry), 9 UNION ALL SELECT CAST('POINT ( 354528.400 6636467.900)' AS geometry), 10 UNION ALL SELECT CAST('POINT (1251250.400 6464646.125)' AS geometry), 11 UNION ALL SELECT CAST('POINT ( 315060.950 6874532.888)' AS geometry), 12 UNION ALL SELECT CAST('POINT ( 415263.920 7077070.707)' AS geometry), 13 UNION ALL SELECT CAST('POINT ( 559988.123 6050300.400)' AS geometry), 14 UNION ALL SELECT CAST('POINT ( 334455.250 6333111.200)' AS geometry), 15 UNION ALL SELECT CAST('POINT ( 312459.258 7172737.333)' AS geometry), 16 ), DISTANCES AS ( SELECT N, P, ID_GEOFLA, INSEE_COM, NOM_COM, GEOM, ST_Distance(P, GEOM) AS D, RANK() OVER(PARTITION BY N ORDER BY ST_Distance(P, GEOM)) AS R FROM S_GEO.COMMUNE CROSS JOIN POINTS ) SELECT *, CASE D WHEN 0 THEN 'intérieur' ELSE 'extérieur' END AS SITUATION FROM DISTANCES WHERE R = 1;",
    "SELECT D1.nom_dept || ' / ' || D2.nom_dept AS NOMS, ST_Union(D1.geom, D2.geom) AS GEO FROM s_geo.departement AS D1 JOIN s_geo.departement AS D2 ON ST_Distance(D1.geom, D2.geom) < 5000 AND ST_Touches(D1.geom, D2.geom) = false AND D1.gid < D2.gid;",
    'SELECT CODE_COM, NOM_COM, C.CODE_DEPT, C.NOM_DEPT, C.GEOM FROM S_GEO.COMMUNE AS C JOIN S_GEO.DEPARTEMENT AS D ON C.CODE_DEPT = D.CODE_DEPT AND ST_Intersects(C.GEOM, ST_Boundary(D.GEOM)) = false WHERE ST_Area(C.GEOM) > 34567890;',
    'SELECT CODE_COM, NOM_COM, C.CODE_DEPT, C.NOM_DEPT, C.GEOM FROM S_GEO.COMMUNE AS C JOIN S_GEO.DEPARTEMENT AS D ON C.CODE_DEPT = D.CODE_DEPT AND ST_Intersects(C.GEOM, ST_Centroid(D.GEOM)) = true;',
    'WITH T AS (SELECT CODE_COM, INSEE_COM, NOM_COM, ST_Area(GEOM) AS SURFACE, GEOM, RANK() OVER(ORDER BY ABS(ST_Area(GEOM) - 6666666)) AS RANK_DIF FROM S_GEO.COMMUNE) SELECT CODE_COM, INSEE_COM, NOM_COM, GEOM, SURFACE FROM T WHERE RANK_DIF = 1;',
    "SELECT CLASS_ADM, SUM(ST_Length(GEOM)) / 1000 AS LONGUEUR_KM FROM S_RTE.TRONCON_ROUTE WHERE CLASS_ADM <> 'Sans objet' GROUP BY CLASS_ADM",
    'SELECT D.CODE_DEPT, NUM_ROUTE, SUM(ST_Length(TR.GEOM)) / 1000 AS LONGUEUR_KM FROM S_RTE.TRONCON_ROUTE AS TR JOIN S_GEO.DEPARTEMENT AS D ON ST_Intersects(TR.GEOM, D.GEOM) = true GROUP BY D.CODE_DEPT, NUM_ROUTE ORDER BY CODE_DEPT, NUM_ROUTE;'
]

optimised_queries = [
    'SELECT *, ST_IsValidDetail(geom) FROM s_adr.code_insee_code_postal WHERE ST_IsValid(geom) = false;',
    'SELECT * FROM   S_GEO.COMMUNE WHERE ST_Area(geom) = (SELECT MAX(ST_Area(GEOM)) FROM S_GEO.COMMUNE)',
    'SELECT CODE_DEPT, ST_Union(GEOM) FROM S_GEO.COMMUNE GROUP BY CODE_DEPT;',
    "WITH points (p, n) AS (VALUES (ST_MakePoint( 430354.933, 6623007.700), 1 ),(ST_MakePoint( 980190.133, 6333111.233), 2 ),(ST_MakePoint( 574865.267, 6909297.167), 3 ),(ST_MakePoint( 501587.200, 6548318.933), 4 ),(ST_MakePoint( 444282.067, 6251421.667), 5 ),(ST_MakePoint( 460953.333, 6775817.633), 6 ),(ST_MakePoint(1032008.400, 6323716.133), 7 ),(ST_MakePoint( 875328.767, 6866887.500), 8 ),(ST_MakePoint( 666398.867, 6560452.500), 9 ),(ST_MakePoint( 354528.400, 6636467.900), 10) ,(ST_MakePoint(1251250.400, 6464646.125), 11) ,(ST_MakePoint( 315060.950, 6874532.888), 12) ,(ST_MakePoint( 415263.920, 7077070.707), 13) ,(ST_MakePoint( 559988.123, 6050300.400), 14) ,(ST_MakePoint( 334455.250, 6333111.200), 15) ,(ST_MakePoint( 312459.258, 7172737.333), 16)) SELECT n,p,id_geofla,insee_com,nom_com,geom, d,RANK() OVER(PARTITION BY n ORDER BY ST_Distance(p, geom)) AS r, CASE d WHEN 0 THEN 'interieur' ELSE 'exterieur' END AS situation FROM points, LATERAL (SELECT id_geofla, insee_com, nom_com, geom, ST_Distance(p, geom) AS d FROM s_geo.commune ORDER BY p <-> s_geo.commune.geom LIMIT 1) sub;",
    "SELECT D1.nom_dept || ' / ' || D2.nom_dept AS NOMS, ST_Union(D1.geom, D2.geom) AS GEO FROM s_geo.departement AS D1 JOIN s_geo.departement AS D2 ON ST_Distance(D1.geom, D2.geom) < 5000 AND ST_Touches(D1.geom, D2.geom) = false AND D1.gid < D2.gid;",
    'SELECT CODE_COM, NOM_COM, C.CODE_DEPT, C.NOM_DEPT, C.GEOM FROM S_GEO.COMMUNE AS C JOIN S_GEO.DEPARTEMENT AS D ON C.CODE_DEPT = D.CODE_DEPT AND ST_ContainsProperly(D.GEOM, C.GEOM) = true WHERE ST_Area(C.GEOM) > 34567890;',
    'WITH D AS (SELECT CODE_DEPT, ST_Centroid(geom) AS GEOM FROM S_GEO.DEPARTEMENT) SELECT CODE_COM, NOM_COM, C.CODE_DEPT, C.NOM_DEPT, C.GEOM FROM S_GEO.COMMUNE AS C JOIN d ON C.CODE_DEPT = D.CODE_DEPT AND ST_Intersects(C.GEOM, d.GEOM) = true;',
    'WITH T AS ( SELECT CODE_COM, INSEE_COM, NOM_COM, ST_Area(GEOM) AS SURFACE, GEOM, RANK() OVER(ORDER BY ABS(ST_Area(GEOM) - 6666666)) AS RANK_DIF FROM S_GEO.COMMUNE) SELECT CODE_COM, INSEE_COM, NOM_COM, GEOM, SURFACE FROM T WHERE RANK_DIF = 1;',
    "SELECT CLASS_ADM, SUM(ST_Length(GEOM)) / 1000 AS LONGUEUR_KM FROM S_RTE.TRONCON_ROUTE WHERE CLASS_ADM <> 'Sans objet' GROUP BY CLASS_ADM",
    'SELECT d.code_dept, routes.num_route, routes.longueur_km FROM s_geo.departement AS d , LATERAL ( SELECT num_route, SUM(ST_Length(tr.geom)) / 1000 AS longueur_km FROM s_rte.troncon_route AS tr WHERE ST_Intersects(TR.GEOM, D.GEOM) GROUP BY d.code_dept, num_route) routes ORDER BY code_dept, num_route;'
]

# exit on Ctrl+C
class graceful_killer:
  kill_now = False
  def __init__(self):
    signal.signal(signal.SIGINT, self.exit_gracefully)
    signal.signal(signal.SIGTERM, self.exit_gracefully)

  def exit_gracefully(self,signum, frame):
    logger.warn("interrupt signal received")
    self.kill_now = True

killer = graceful_killer()
    
def main():
    usage = "usage: %prog [options]"

    parser = optparse.OptionParser(usage=usage)

    parser.add_option("-d", "--database", dest="database_name",
                      help="the database name", default="db_geo")
    parser.add_option("-o", "--output", dest="output_file",
                      help="the file where to output the result",
                      default="/tmp/run_tests.csv")
    parser.add_option("-t", "--test", dest="run_tests",
                      action="store_true", default=False,
                      help="do we run the SQL tests")
    parser.add_option("--optim", dest="run_optimised_queries",
                      action="store_true", default=False,
                      help="do we run the PostgreSQL optimised queries")
    parser.add_option("-c", "--configure", dest="configure_db",
                      action="store_true", default=False,
                      help="do we configure the database")
    parser.add_option("-s", "--shared_buffers", dest="shared_buffers",
                      help="set shared_buffers parameter", default=False)
    parser.add_option("-w", "--work_mem", dest="work_mem",
                      help="set work_mem parameter", default=False)
    parser.add_option("--maintenance", dest="maintenance_work_mem",
                      help="set maintenance_work_mem parameter", default=False)
    parser.add_option("-m", "--max_workers", dest="max_workers",
                      help="set max_parallel_workers_per_gather parameter",
                      default=False)
    parser.add_option("-r", "--restore", dest="restore_database",
                      action="store_true", default=False,
                      help="do we restore the database")
    parser.add_option("-b", "--backup", dest="backup_database",
                      help="do we run the backup test", default=False)
    parser.add_option("-P", "--purge", dest="purge_backups",
                      action="store_true", default=False,
                      help="do we remove the created backups")
    parser.add_option("-j", "--jobs_number", dest="jobs_number",
                      help="job number for backup/restore", default="1")
    parser.add_option("-D", "--drop", dest="drop_database",
                      action="store_true", default=False,
                      help="do we drop the database")
    parser.add_option("-q", "--quiet", dest="quiet", action="store_true", 
                      help="do not output information", default=False)
    parser.add_option("--debug", dest="debug", action="store_true", 
                      help="log all bash command call", default=False)

    (options, args) = parser.parse_args()

    # set log level
    if options.debug:
        global log_bash_cmd
        log_bash_cmd = True
        LOG_LEVEL=logging.DEBUG
    if options.quiet:
        LOG_LEVEL=logging.WARN
    if options.debug or options.quiet:
        logger.setLevel(LOG_LEVEL)
        logger_ch.setLevel(LOG_LEVEL)
        
    # rename the output file if already exists
    if os.path.isfile(options.output_file):
        os.rename(options.output_file, options.output_file
                  +"_"+dt.datetime.now().strftime("%y%m%d%H%M%S"))
            
    # configure the database
    if options.configure_db:
        config_db(options.shared_buffers, options.work_mem,
                  options.maintenance_work_mem, options.max_workers)
        logger.info("database configuration done")

    # get the current configuration
    db_config = get_db_config(options.jobs_number)
    with open(options.output_file, 'w') as f:
        f.write(db_config)
        
    # create and restore database
    if options.restore_database:
        drop_db(options.database_name)
        create_db(options.database_name)
        duration = restore_db(options.database_name, options.jobs_number)
        with open(options.output_file, 'a+') as f:
            f.write("custom restore;{0:.0f}\n".format(duration))
        
    # run the tests
    if options.run_tests:
        logger.info("start running tests")
        queries = original_queries
        if options.run_optimised_queries:
          queries = optimised_queries
        tests_result = run_parallel_tests(options.database_name,
                                          int(options.jobs_number),
                                          queries)
        if killer.kill_now:
            logger.info("exit gracefully")
        with open(options.output_file, 'a+') as f:
            f.write("SQL tests\n")
            for query_nb,duration in tests_result:
                f.write("Test {0};{1:.0f}\n".format(query_nb,duration))
        logger.info("all tests run")
        
    # backup the database 
    if options.backup_database:
        bck_res = backup_db(options.database_name, options.backup_database,
                            options.jobs_number, options.purge_backups)
        with open(options.output_file, 'a+') as f:
            f.write("{0} backup;{1:.0f}\n".format(bck_res[0], bck_res[1]))
        
    # drop the database
    if options.drop_database:
        drop_db(options.database_name)
        logger.info("database dropped")


def run_bash_command(command_line):
    if log_bash_cmd:
        logger.debug("run_bash_command: '{0}'".format(command_line))
    res = subprocess.check_output(shlex.split(command_line))
    if log_bash_cmd:
        logger.debug("run_bash_command result: '{0}'".format(res))
    return res

def get_human_readable(size,precision=2):
    suffixes=['B','KB','MB','GB','TB']
    suffixIndex = 0
    while size > 1024 and suffixIndex < 4:
        suffixIndex += 1 #increment the index of the suffix
        size = size/1024.0 #apply the division
    return "%.*f%s"%(precision,size,suffixes[suffixIndex])

def get_db_config(jobs_number):
    def get_pg_config_param(key):
        result = run_bash_command("psql --quiet -t -c 'SHOW {0}'".format(key))
        return "{0};{1}\n".format(key,result.strip())
    
    result = "Config\n"
    result += "CPU number;{0}\n".format(psutil.cpu_count())
    result += "RAM;{0}\n".format(get_human_readable(psutil.virtual_memory().total,1))
    result += get_pg_config_param("shared_buffers")
    result += get_pg_config_param("work_mem")
    result += get_pg_config_param("maintenance_work_mem")
    result += get_pg_config_param("max_parallel_workers_per_gather")
    result += "Parallel clients;{0}\n".format(jobs_number)
    return result

def config_db(shared_buffers, work_mem, maintenance_work_mem, max_workers):
    def config_pg_param(key, value):
        if value:
            run_bash_command("psql -c 'ALTER SYSTEM SET {0} TO \"{1}\"'".format(key, value))

    config_pg_param('shared_buffers', shared_buffers)
    config_pg_param('work_mem', work_mem)
    config_pg_param('maintenance_work_mem', maintenance_work_mem)
    config_pg_param('max_parallel_workers_per_gather', max_workers)
    run_bash_command('sudo systemctl restart postgresql')

def create_db(database_name):
    run_bash_command('createdb {0}'.format(database_name))
    
def restore_db(database_name, jobs_number):
    n1=dt.datetime.now()
    run_bash_command("pg_restore -j{0} -d {1} db_geo_dump".format(jobs_number, database_name))
    duration=(dt.datetime.now()-n1).total_seconds()*1000
    logger.debug("custom restore duration : {0:.0f} ms".format(duration))
    return duration

def run_test(database_name, query_nb, query):
    """
    Run the given query number once to help fill the cache, then run it 10 
    times.
    """
    bash_command = "psql --quiet -c 'SET client_min_messages=WARNING' -c '\\timing' -c '\\o /dev/null' -c \"{0}\" {1}".format(query, database_name)
    
    try:
        logger.debug("query n°{0} result: {1}".format(query_nb, run_bash_command(bash_command)))
    except Exception as e:
        logger.warn("error in query n°{0}: '{2}' ({3}".format(query_nb, i, msg, e))
        return -1
    
    result=[]
    # run 10 times each query
    for i in range(0,10):
        if killer.kill_now:
            return -1
        try:
            msg = run_bash_command(bash_command)
            m = re.search(' [0-9,.]* ', msg)
            res = float(m.group(0).strip().replace(',','.'))
            logger.debug("run query n°{0} res {1}: '{2}'".format(query_nb, i, res))
            result.append(res)
        except Exception as e:
            logger.warn("error in query n°{0}-{1}: '{2}' ({3})".format(query_nb, i, msg, e))
    logger.debug("query n°{0} durations: {1}".format(query_nb, result))
    return result
    
def run_tests(database_name, queries):
    """
    run the SQL queries in the 'queries' array.
    Compute the result on the average removing the fastest and slowest single
    result.
    """
    tests_result = []
    for i, query in enumerate(queries):
        if killer.kill_now:
            return tests_result
        res = run_test(database_name, i+1, query)
        mean = math.fsum(sorted(res)[1:-1])/len(res[1:-1])
        tests_result.append([i+1,mean])
        logger.info("query n°{0} average duration: {1:.0f} ms".format(i+1, mean))
    logger.debug("run_test result: '{0}'".format(tests_result))
    return tests_result

def run_test_args(args):
    return run_test(*args)

def run_parallel_tests(database_name, parallel_query_nb, queries):
    """
    run in parallel the same SQL queries in the 'queries' array.
    Compute the result on the average removing the fastest and slowest single
    result from each worker.
    """
    pool = mp.Pool(parallel_query_nb)
    all_tests_result = []
    for i, query in enumerate(queries):
        if killer.kill_now:
            pool.terminate()
            pool.join()
            break
        # create the arguments parallel_query_nb times
        tasks = [(database_name, i+1, query)]
        tasks *= parallel_query_nb
        try:
            test_results = pool.map(run_test_args, tasks)
        except Exception as e:
            killer.kill_now = True
            logger.warn("error in query n°{0}: '{1}'".format(i+1, e))
        all_res = []
        for t_res in test_results:
            all_res.extend(t_res)
        mean = math.fsum(sorted(all_res)[1:-1])/len(all_res[1:-1])
        all_tests_result.append([i+1,mean])
        logger.info("query n°{0} average duration: {1:.0f} ms".format(i+1, mean))

    logger.info("run_parallel_tests result: '{0}'".format(all_tests_result))
    return all_tests_result
    

def backup_db(database_name, backup_mode, jobs_number, remove_backups=True):
    """ 
    backup in the database in plain SQL, custom or directory mode.
    First remove the old dump then make the backup and possibly remove the dump.
    Returns a pair with backup mode and duration
    """

    n1=dt.datetime.now()
    if backup_mode in [ "p", "plain"] :
        backup_mode = "plain"
        try: os.remove("/tmp/db_geo.sql")
        except: pass
        run_bash_command("pg_dump -f/tmp/db_geo.sql {0}".format(database_name))
        if remove_backups:
            os.remove("/tmp/db_geo.sql")
    
    elif backup_mode in [ "c", "custom"] :
        backup_mode = "custom"
        try: os.remove("/tmp/db_geo.dump")
        except: pass
        run_bash_command("pg_dump -Fc -f/tmp/db_geo.dump {0}".format(database_name))
        if remove_backups:
            os.remove("/tmp/db_geo.dump")

    elif backup_mode in ["d", "dir", "directory"] :
        backup_mode = "directory"
        try: shutil.rmtree("/tmp/db_geo_dump")
        except: pass
        run_bash_command("pg_dump -j{0} -Fd -f/tmp/db_geo_dump {1}".format(jobs_number, database_name))
        if remove_backups:
            shutil.rmtree("/tmp/db_geo_dump")

    else:
        backup_mode = "none"
        logger.warn("'{0}' mode for backup unknown, use 'p', 'c' or 'd'".format(backup_mode))
        return ( backup_mode, -1 )

    duration=(dt.datetime.now()-n1).total_seconds()*1000
    logger.info("{0} backup duration : {1:.0f} ms".format(backup_mode, duration))
    return ( backup_mode, duration )
        
def drop_db(database_name):
    run_bash_command('dropdb --if-exists {0}'.format(database_name))

        
if __name__ == "__main__":
    main()
